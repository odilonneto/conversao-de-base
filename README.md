# Algoritmo para Conversão de Base - Método Divisão Sucessivas

## Descrição


## Como utilizar o CONVERSOR DE BASE

Para usar a biblioteca adicione o `trocaBase.h` no seu código:

```c
#include <stdio.h>
#include "trocaBase.h"
```

Para rodar exemplo, use o seguinte comando:

```bash
gcc ./src/main.c -I ./Include/ -o main
./main
```


## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9982723/avatar.png?width=400)  | Odilon Neto | odilonneto | [odilonneto@alunos.utfpr.edu.br](mailto:odilonneto@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9906081/avatar.png?width=400)  | Mateus Rigon | MateusRlink | [mateuslink@alunos.utfpr.edu.br](mailto:mateuslink@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9980149/avatar.png?width=400)  | Leonardo Capelin |Leonardo_Capelin| [leonardocapelin@alunos.utfpr.edu.br](mailto:leonardocapelin@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9867470/avatar.png?width=400)  | Felipe Navarro | Chomray | [blasques@alunos.utfpr.edu.br](mailto:blasques@alunos.utfpr.edu.br)





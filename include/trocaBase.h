#include <stdio.h>
#include <math.h>

int decimalBinario(float a)
{
   int num, aux, test[8];
   
   num = a;

   for (aux = 7; aux >= 0; aux--)
   {
      if (num % 2 == 0)
         test[aux] = 0;
      else
         test[aux] = 1;
      num = num / 2;
   }

   for (aux = 0; aux < 8; aux++)
       printf("%d", test[aux]);

}


int binarioDecimal(int a)
{
    int num, decimal = 0, i;
    
    num = a;
    
    for(i = 0; num > 0; i++)
    {
        decimal = decimal + (i * i) * (num % 10);
        num = num / 10;
    }
    printf("%d\n", decimal);
    return 0;
}
